const cors = require("cors");
const bodyParser = require("body-parser");
const express = require("express");
var multipart = require('connect-multiparty');

const app = express();

global.__basedir = __dirname;

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

app.use(express.json())
// parse requests of content-type - application/json
app.use(bodyParser.json());
const db = require("./src/models");
db.sequelize.sync();
require("./src/routes/user.routes")(app);

app.use(express.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to application." });
});

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

let url = 'http://localhost:8080/';
module.exports = url;