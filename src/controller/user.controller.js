const uploadFile = require("../middleware/upload");
const db = require("../models");
const server = require("../../server");
const User = db.users;
const Op = db.Sequelize.Op;
var formidable = require('formidable');
const sharp = require("sharp");
const fs = require("fs");

const create = async (req, res, next)=> {
  let user = {};
  let form = new formidable.IncomingForm();
  try {
   form.parse(req, function(err, fields) {
    // `file` is the name of the <input> field of type `file`
    if (!fields.first_name||!fields.last_name||!fields.email) {
      res.status(400).send({
        message: `Content can not be empty! ${req}`
      });
      return;
    }
      user = {
        first_name: fields.first_name,
        last_name: fields.last_name,
        email: fields.email,
    };
  });

   /* await sharp(req.file.path)
      .resize(200, 200)
      .jpeg({ quality: 90 })
      .toFile(
        path.resolve(req.file.destination,'resized',image)
      )
    fs.unlinkSync(req.file.path)*/

    await uploadFile(req, res)

    if (req.file == undefined) {
      return res.status(400).send({ message: "Please upload a file!" });
    }

    user.photo = `${req.get('host')}/users/resources/static/assets/uploads/${req.file.originalname}`
  } catch (err) {
    res.status(500).send({
      message: `Could not upload the file: ${req.file.originalname}. ${err}`,
    });
  }
  User.create(user)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    });
}


// Retrieve all Users from the database.
const findAll = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.iLike]: `%${title}%` } } : null;

  User.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
};

// Find a single User with an id
const findOne = (req, res) => {
  const id = req.params.id;

  User.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving User with id=" + id
      });
    });
};

// Update a User by the id in the request
const update = (req, res) => {
  const id = req.params.id;

  User.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating User with id=" + id
      });
    });
};

// Delete a User with the specified id in the request
const deleteOne = (req, res) => {
  const id = req.params.id;

  User.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete User with id=${id}. Maybe User was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete User with id=" + id
      });
    });
};

// Delete all Users from the database.
const deleteAll = (req, res) => {
  User.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Users were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all users."
      });
    });
};

module.exports = {
  create,
  findAll,
  findOne,
  update,
  deleteOne,
  deleteAll
};
